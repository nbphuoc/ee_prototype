class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.decimal :price, precision: 16, scale: 8
      t.decimal :amount, precision: 16, scale: 8
      t.string :order_type

      t.timestamps
    end
  end
end
