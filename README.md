# README #
This is a prototype for EE

### What does this prototype use? ###
* [Pure](http://purecss.io/)
* [Mustache](https://github.com/janl/mustache.js)
* [Jquery](https://github.com/jquery/jquery)

### Pure ###
* Pros:
    * Lightweight
    * Can add more functionality if needed
* Cons:
    * Dosen't have much functionality

### Mustache ###
* Pros:
    * Lightweight
* Cons:
    * Don't have filters, directive,... like angularjs
* Possible replacement: 
    * [Handdlebars](http://handlebarsjs.com/) With basic filters support

### Jquery ###
* Pros:
   * Easier to use and manage
   * Big community
* Cons:
    * Hard to handle when javascript front-end get bigger