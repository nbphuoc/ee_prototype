class Api::V1::OrderController < ApplicationController
  def create
    permited_params = params.require(:order).permit(:price, :amount, :order_type)
    render json: {success: Order.create(permited_params)}
  end

  def index
    render json: {success: true, orders: Order.all.order("id DESC")}
  end
end
