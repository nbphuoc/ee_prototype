// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function() {
  $("#buy_form").on("submit", function(e) {
    e.preventDefault();
    var formData = $(this).serializeObject();
    formData.order_type = "buy";
    submitOrder(formData);
  });
  $("#sell_form").on("submit", function(e) {
    e.preventDefault();
    var formData = $(this).serializeObject();
    formData.order_type = "sell";
    submitOrder(formData);
  });

  submitOrder = function(data) {
    $.ajax({
      type:'POST',
      url: '/api/v1/order',
      data: {order: data},
      success: function(res) {
        if (res.success) {
          console.log("Order added");
          getOrder();
        } else {
          alert("ERROR");
        }
      },
      error: function() {
        alert("ERROR");
      }
    });
  }

  getOrder = function() {
    $.ajax({
      type:'GET',
      url: '/api/v1/order',
      success: function(res) {
        updateOrderTable(res.orders);
      },
      error: function() {
        alert(error)
      }
    });
  }

  updateOrderTable = function(orders) {
    console.log(orders);
    var template = $("#table_template").html();
    Mustache.parse(template); //For faster future uses
    order = {
      price: 1,
      amount: 2,
      order_type: "sell"
    }
    var rendered = Mustache.render(template, {orders: orders});
    console.log(rendered);
    $("#ladder_place").html(rendered);
  }

  getOrder();
});
